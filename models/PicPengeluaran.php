<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "pic_pengeluaran".
 *
 * @property integer $id
 * @property integer $id_pic
 * @property string $tanggal
 * @property string $keperluan
 * @property integer $nominal
 * @property integer $banyaknya
 * @property string $catatan
 *
 * @property Pic $pic
 */
class PicPengeluaran extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    
    public static function tableName()
    {
        return 'pic_pengeluaran';
    }

    public function rules()
    {
        return [
            //id

            //id_pic
            [['id_pic'], 'required'],
            [['id_pic'], 'integer'],
            [['id_pic'], 'exist', 'skipOnError' => true, 'targetClass' => Pic::className(), 'targetAttribute' => ['id_pic' => 'id']],

            //tanggal
            [['tanggal'], 'safe'],

            //keperluan
            [['keperluan'], 'string'],

            //nominal
            [['nominal'], 'integer'],

            //banyaknya
            [['banyaknya'], 'integer'],

            //catatan
            [['catatan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pic' => 'Id Pic',
            'tanggal' => 'Tanggal',
            'keperluan' => 'Keperluan',
            'nominal' => 'Nominal',
            'banyaknya' => 'Banyaknya',
            'catatan' => 'Catatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPic()
    {
        return $this->hasOne(Pic::className(), ['id' => 'id_pic']);
    }
}
