<?php
namespace app_tryout\models;

use Yii;
use yii\base\Model;
use app_tryout\models\ReferralAgent;

class LoginReferral extends Model
{
    public $kode;
    public $handphone;
    public $rememberMe = true;

    public $referralAgent;

    public function rules()
    {
        return [
            ['kode', 'required', 'on' => 'using-kode'],
            
            ['handphone', 'required'],
            ['handphone', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'handphone' => 'Password',
            'rememberMe' => 'Remember Me',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if ($referralAgent = $this->getReferralAgent()) {
            if (!$referralAgent->validatePassword($this->handphone)) {
                $this->addError($attribute, 'Incorrect handphone.');
            }
        } else {
            switch ($this->scenario) {
                case 'using-kode':
                    $this->addError('kode', 'Kode is not exists.');
                    break;
                default:
                    $this->addError('kode', 'Kode is not exists.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->userReferral->login($this->getReferralAgent(), $this->rememberMe ? Yii::$app->params['user.passwordResetTokenExpire'] * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    protected function getReferralAgent()
    {
        if ($this->referralAgent === null) {
            switch ($this->scenario) {
                case 'using-kode':
                    $this->referralAgent = ReferralAgent::findByKode($this->kode);
                    break;
                default:
                    $this->referralAgent = ReferralAgent::findByKode($this->kode);
            }
        }

        return $this->referralAgent;
    }
}