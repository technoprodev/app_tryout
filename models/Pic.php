<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "pic".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property integer $id_periode_kota
 * @property string $kode
 * @property string $username
 * @property string $status
 * @property string $nama
 * @property string $email
 * @property string $handphone
 * @property string $whatsapp
 * @property string $nomor_rekening
 * @property string $nama_bank
 * @property string $atas_nama
 * @property string $alamat_pengiriman
 * @property string $nama_penerima
 * @property string $telpon_penerima
 * @property string $ukuran_kaos
 * @property string $nama_jasa_pengiriman
 * @property string $nomor_resi
 * @property string $catatan_pengiriman
 * @property string $rangkuman_kegiatan
 * @property string $link_foto
 * @property string $laporan_keuangan
 * @property string $note_khusus
 * @property string $file_khusus
 * @property string $acara_lain_diluar_tryout
 * @property string $uang_ini_ada_dimana
 * @property string $catatan_laporan_keuangan
 * @property string $catatan_pemasukan
 * @property integer $jumlah_pendaftar
 * @property integer $harga_satuan_pendaftar
 * @property string $catatan_pendaftar
 * @property string $catatan_pengeluaran
 * @property string $yang_diberikan_ke_volunteer
 * @property integer $jumlah_volunteer_hari_acara
 * @property integer $fee_satuan_volunteer
 * @property string $catatan_volunteer
 * @property string $acara_lain_diluar_tryout_sudah_final
 *
 * @property PeriodeKota $periodeKota
 * @property Periode $periode
 * @property PicPemasukan[] $picPemasukans
 * @property PicPengeluaran[] $picPengeluarans
 * @property Volunteer[] $volunteers
 */
class Pic extends \technosmart\yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $virtual_laporan_keuangan_upload;
    public $virtual_laporan_keuangan_download;
    public $virtual_file_khusus_upload;
    public $virtual_file_khusus_download;

    public static function tableName()
    {
        return 'pic';
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //id_periode_kota
            [['id_periode_kota'], 'required'],
            [['id_periode_kota'], 'integer'],
            [['id_periode_kota'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeKota::className(), 'targetAttribute' => ['id_periode_kota' => 'id']],

            //kode
            [['kode'], 'required'],
            [['kode'], 'string', 'max' => 16],

            //username
            [['username'], 'required'],
            [['username'], 'string', 'max' => 16],

            //status
            [['status'], 'string'],

            //nama
            [['nama'], 'string', 'max' => 256],

            //email
            [['email'], 'string', 'max' => 256],

            //handphone
            [['handphone'], 'number', 'message' => '{attribute} harus angka'],
            [['handphone'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['handphone'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //whatsapp
            [['whatsapp'], 'string', 'max' => 16],

            //nomor_rekening
            [['nomor_rekening'], 'string', 'max' => 64],

            //nama_bank
            [['nama_bank'], 'string', 'max' => 64],

            //atas_nama
            [['atas_nama'], 'string', 'max' => 64],

            //alamat_pengiriman
            [['alamat_pengiriman'], 'string'],

            //nama_penerima
            [['nama_penerima'], 'string'],

            //telpon_penerima
            [['telpon_penerima'], 'number', 'message' => '{attribute} harus angka'],
            [['telpon_penerima'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['telpon_penerima'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //ukuran_kaos
            [['ukuran_kaos'], 'string', 'max' => 4],

            //nama_jasa_pengiriman
            [['nama_jasa_pengiriman'], 'string', 'max' => 256],

            //nomor_resi
            [['nomor_resi'], 'string', 'max' => 256],

            //catatan_pengiriman
            [['catatan_pengiriman'], 'string'],

            //rangkuman_kegiatan
            [['rangkuman_kegiatan'], 'string'],

            //link_foto
            [['link_foto'], 'string'],

            //laporan_keuangan
            [['laporan_keuangan'], 'string'],

            //note_khusus
            [['note_khusus'], 'string'],

            //file_khusus
            [['file_khusus'], 'string'],

            //acara_lain_diluar_tryout
            [['acara_lain_diluar_tryout'], 'string'],

            //uang_ini_ada_dimana
            [['uang_ini_ada_dimana'], 'string'],

            //catatan_laporan_keuangan
            [['catatan_laporan_keuangan'], 'string'],

            //catatan_pemasukan
            [['catatan_pemasukan'], 'string'],

            //jumlah_pendaftar
            [['jumlah_pendaftar'], 'integer'],

            //harga_satuan_pendaftar
            [['harga_satuan_pendaftar'], 'integer'],

            //catatan_pendaftar
            [['catatan_pendaftar'], 'string'],

            //catatan_pengeluaran
            [['catatan_pengeluaran'], 'string'],

            //yang_diberikan_ke_volunteer
            [['yang_diberikan_ke_volunteer'], 'string'],

            //jumlah_volunteer_hari_acara
            [['jumlah_volunteer_hari_acara'], 'integer'],

            //fee_satuan_volunteer
            [['fee_satuan_volunteer'], 'integer'],

            //catatan_volunteer
            [['catatan_volunteer'], 'string'],

            //acara_lain_diluar_tryout_sudah_final
            [['acara_lain_diluar_tryout_sudah_final'], 'string'],
            
            //virtual_laporan_keuangan_download
            [['virtual_laporan_keuangan_download'], 'safe'],
            
            //virtual_laporan_keuangan_upload
            [['virtual_laporan_keuangan_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal upload 3MB'],
            
            //virtual_file_khusus_download
            [['virtual_file_khusus_download'], 'safe'],
            
            //virtual_file_khusus_upload
            [['virtual_file_khusus_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal upload 3MB'],
        ];
    }

    public static function findByKode($kode)
    {
        return static::find()
            ->join('INNER JOIN', 'periode p', 'p.id = pic.id_periode')
            ->where('pic.kode = :kode', [':kode' => $kode])
            ->andWhere(['pic.status' => 'Sedang Aktif'])
            ->andWhere(['p.id' => (Periode::getPeriodeAktif())->id])
            ->one();
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id/*, 'status' => 'Aktif'*/]);
    }

    /**
     * Login secara API based
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['password' => $token/*, 'status' => 'Aktif'*/]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->username;
    }

    /**
     * Membandingkan Authkey existing dengan Authkey yang ada di browser
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->username === $password;
    }

    //
    
    public function beforeValidate()
    {
        $this->virtual_laporan_keuangan_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_laporan_keuangan_upload');
        if ($this->virtual_laporan_keuangan_upload) {
            $this->laporan_keuangan = $this->virtual_laporan_keuangan_upload->name;
            // $this->laporan_keuangan = $this->kode . '.' . $this->virtual_laporan_keuangan_upload->extension;
        }
        $this->virtual_file_khusus_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_file_khusus_upload');
        if ($this->virtual_file_khusus_upload) {
            $this->file_khusus = $this->virtual_file_khusus_upload->name;
            // $this->file_khusus = $this->kode . '.' . $this->virtual_file_khusus_upload->extension;
        }
        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_laporan_keuangan_upload) {
            if (!$insert) {
                $fileRoot = '@upload-pic-laporan_keuangan';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->laporan_keuangan;
                if (is_file($filePath)) unlink($filePath);
            }
        }
        if ($this->virtual_file_khusus_upload) {
            if (!$insert) {
                $fileRoot = '@upload-pic-file_khusus';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->file_khusus;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_laporan_keuangan_upload) {
            $uploadRoot = '@upload-pic-laporan_keuangan';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_laporan_keuangan_upload->saveAs($path . '/' . $this->laporan_keuangan);
        }
        if ($this->virtual_file_khusus_upload) {
            $uploadRoot = '@upload-pic-file_khusus';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_file_khusus_upload->saveAs($path . '/' . $this->file_khusus);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = '@upload-pic-laporan_keuangan';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->laporan_keuangan;
        if (is_file($filePath)) unlink($filePath);

        $fileRoot = '@upload-pic-file_khusus';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->file_khusus;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->laporan_keuangan) {
            $downloadBaseUrl = '@download-pic-laporan_keuangan';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_laporan_keuangan_download = $path . '/' . $this->laporan_keuangan;
        }
        if($this->file_khusus) {
            $downloadBaseUrl = '@download-pic-file_khusus';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_file_khusus_download = $path . '/' . $this->file_khusus;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Periode',
            'id_periode_kota' => 'Periode Kota',
            'kode' => 'Kode',
            'username' => 'Username',
            'status' => 'Status',
            'nama' => 'Nama Lengkap PIC',
            'email' => 'Email',
            'handphone' => 'Handphone',
            'whatsapp' => 'Whatsapp',
            'nomor_rekening' => 'Nomor Rekening',
            'nama_bank' => 'Nama Bank',
            'atas_nama' => 'Atas Nama Pemilik Rekening',
            'alamat_pengiriman' => 'Alamat Pengiriman Logistik Tryout',
            'nama_penerima' => 'Nama Penerima Kiriman Logistik',
            'telpon_penerima' => 'No. Telp Penerima Kiriman Logistik',
            'ukuran_kaos' => 'Ukuran Kaos PIC',
            'nama_jasa_pengiriman' => 'Nama Jasa Pengiriman',
            'nomor_resi' => 'Nama Jasa Pengiriman',
            'catatan_pengiriman' => 'Catatan Pengiriman',
            'rangkuman_kegiatan' => 'Rangkuman Kegiatan',
            'link_foto' => 'Link Foto',
            'laporan_keuangan' => 'Laporan Keuangan',
            'note_khusus' => 'Note Khusus',
            'file_khusus' => 'File Khusus',
            'acara_lain_diluar_tryout' => 'Acara Lain Diluar Tryout',
            'uang_ini_ada_dimana' => 'Uang Ini Ada Dimana',
            'catatan_laporan_keuangan' => 'Catatan Laporan Keuangan',
            'catatan_pemasukan' => 'Catatan Pemasukan',
            'jumlah_pendaftar' => 'Jumlah Pendaftar',
            'harga_satuan_pendaftar' => 'Harga Satuan Pendaftar',
            'catatan_pendaftar' => 'Catatan Pendaftar',
            'catatan_pengeluaran' => 'Catatan Pengeluaran',
            'yang_diberikan_ke_volunteer' => 'Yang Diberikan Ke Volunteer',
            'jumlah_volunteer_hari_acara' => 'Jumlah Volunteer Hari Acara',
            'fee_satuan_volunteer' => 'Fee Satuan Volunteer',
            'catatan_volunteer' => 'Catatan Volunteer',
            'acara_lain_diluar_tryout_sudah_final' => 'Acara Lain Diluar Tryout Sudah Final ?',
            'virtual_laporan_keuangan_download' => 'Laporan Keuangan',
            'virtual_laporan_keuangan_upload' => 'Laporan Keuangan',
            'virtual_file_khusus_download' => 'File Khusus',
            'virtual_file_khusus_upload' => 'File Khusus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeKota()
    {
        return $this->hasOne(PeriodeKota::className(), ['id' => 'id_periode_kota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPicPemasukans()
    {
        return $this->hasMany(PicPemasukan::className(), ['id_pic' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPicPengeluarans()
    {
        return $this->hasMany(PicPengeluaran::className(), ['id_pic' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteers()
    {
        return $this->hasMany(Volunteer::className(), ['id_pic' => 'id']);
    }
}
