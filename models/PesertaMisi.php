<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "peserta_misi".
 *
 * @property integer $id
 * @property integer $id_peserta
 * @property integer $id_misi
 * @property string $status_konfirmasi
 * @property string $poto
 * @property string $catatan_peserta
 * @property string $catatan_admin
 *
 * @property Peserta $peserta
 */
class PesertaMisi extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_poto_upload;
    public $virtual_poto_download;

    public static function tableName()
    {
        return 'peserta_misi';
    }

    public function rules()
    {
        return [
            //id

            //id_peserta
            [['id_peserta'], 'required'],
            [['id_peserta'], 'integer'],
            [['id_peserta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['id_peserta' => 'id']],

            //id_misi
            [['id_misi'], 'required'],
            [['id_misi'], 'integer'],

            //status_konfirmasi
            [['status_konfirmasi'], 'required'],
            [['status_konfirmasi'], 'string'],

            //poto
            [['poto'], 'required'],
            [['poto'], 'string', 'max' => 256],

            //catatan_peserta
            [['catatan_peserta'], 'string'],

            //catatan_admin
            [['catatan_admin'], 'string'],

            //virtual_poto_download
            [['virtual_poto_download'], 'safe'],
            
            //virtual_poto_upload
            [['virtual_poto_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal screenshot bukti bayar adalah 3MB'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_poto_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_poto_upload');
        if ($this->virtual_poto_upload) {
            $this->poto = $this->virtual_poto_upload->name;
            // $this->poto = $this->kode . '.' . $this->virtual_poto_upload->extension;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_poto_upload) {
            if (!$insert) {
                $fileRoot = '@upload-peserta_misi-poto';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->poto;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_poto_upload) {
            $uploadRoot = '@upload-peserta_misi-poto';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_poto_upload->saveAs($path . '/' . $this->poto);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = '@upload-peserta_misi-poto';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->poto;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->poto) {
            $downloadBaseUrl = '@download-peserta_misi-poto';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_poto_download = $path . '/' . $this->poto;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_peserta' => 'Id Peserta',
            'id_misi' => 'Id Misi',
            'status_konfirmasi' => 'Status Konfirmasi',
            'poto' => 'Poto',
            'catatan_peserta' => 'Catatan Peserta',
            'catatan_admin' => 'Catatan Admin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeserta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'id_peserta']);
    }
}
