<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "peserta_hadiah".
 *
 * @property integer $id
 * @property integer $id_peserta
 * @property integer $id_hadiah
 *
 * @property Peserta $peserta
 * @property Hadiah $hadiah
 */
class PesertaHadiah extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'peserta_hadiah';
    }

    public function rules()
    {
        return [
            //id

            //id_peserta
            [['id_peserta'], 'required'],
            [['id_peserta'], 'integer'],
            [['id_peserta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['id_peserta' => 'id']],

            //id_hadiah
            [['id_hadiah'], 'required'],
            [['id_hadiah'], 'integer'],
            [['id_hadiah'], 'exist', 'skipOnError' => true, 'targetClass' => Hadiah::className(), 'targetAttribute' => ['id_hadiah' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_peserta' => 'Id Peserta',
            'id_hadiah' => 'Id Hadiah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeserta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'id_peserta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHadiah()
    {
        return $this->hasOne(Hadiah::className(), ['id' => 'id_hadiah']);
    }
}
