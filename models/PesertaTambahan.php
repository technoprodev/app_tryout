<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "peserta_tambahan".
 *
 * @property integer $id
 * @property integer $id_peserta
 * @property integer $kode
 * @property string $nama
 * @property string $email
 * @property string $handphone
 * @property integer $id_periode_jenis
 * @property integer $harga
 * @property string $periode_penjualan
 * @property integer $id_periode_kota
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $line
 * @property string $whatsapp
 * @property string $status_survey_pembahasan
 * @property string $status_survey_pengumuman
 * @property string $kode_ptngo
 *
 * @property Peserta $peserta
 * @property PeriodeJenis $periodeJenis
 * @property PeriodeKota $periodeKota
 * @property PesertaSurveyPembahasan[] $pesertaSurveyPembahasans
 */
class PesertaTambahan extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $virtual_survey_pembahasan = [];
    public $virtual_survey_pengumuman = [];
    
    public static function tableName()
    {
        return 'peserta_tambahan';
    }

    public function rules()
    {
        return [
            //id

            //id_peserta
            [['id_peserta'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_peserta'], 'integer'],
            [['id_peserta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['id_peserta' => 'id']],

            //kode
            [['kode'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['kode'], 'integer', 'message' => '{attribute} harus angka'],

            //nama
            [['nama'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['nama'], 'string', 'max' => 256],

            //email
            [['email'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['email'], 'string', 'max' => 256],
            [['email'], 'match', 'pattern' => '/^[\w.+\-]+@(aol.com|gmail.com|hotmail.com|live.com|outlook.com|[\w.+\-]+sch.id|yahoo.co.id|yahoo.com|ymail.com)$/', 'message' => '{attribute} yang diperbolehkan hanya aol.com, gmail.com, hotmail.com, live.com, outlook.com, sch.id, co.id, yahoo.com, ymail.com'],

            //handphone
            [['handphone'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['handphone'], 'number', 'message' => '{attribute} harus angka'],
            [['handphone'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['handphone'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //id_periode_jenis
            [['id_periode_jenis'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_periode_jenis'], 'integer'],
            [['id_periode_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeJenis::className(), 'targetAttribute' => ['id_periode_jenis' => 'id']],

            //harga
            [['harga'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['harga'], 'integer'],

            //periode_penjualan
            [['periode_penjualan'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['periode_penjualan'], 'string', 'max' => 256],

            //id_periode_kota
            [['id_periode_kota'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_periode_kota'], 'integer'],
            [['id_periode_kota'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeKota::className(), 'targetAttribute' => ['id_periode_kota' => 'id']],

            //facebook
            [['facebook'], 'string', 'max' => 256],

            //twitter
            [['twitter'], 'string', 'max' => 256],

            //instagram
            [['instagram'], 'string', 'max' => 256],

            //line
            [['line'], 'string', 'max' => 256],

            //whatsapp
            [['whatsapp'], 'number', 'message' => '{attribute} harus angka'],
            [['whatsapp'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['whatsapp'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //status_survey_pembahasan
            [['status_survey_pembahasan'], 'string'],

            //status_survey_pengumuman
            [['status_survey_pengumuman'], 'string'],

            //kode_ptngo
            [['kode_ptngo'], 'string'],
            
            //virtual_survey_pembahasan
            [['virtual_survey_pembahasan'], 'safe'],
            // [['virtual_survey_pembahasan'], 'each', 'rule' => ['required', 'on' => 'pembahasan', 'message' => '{attribute} tidak boleh kosong'], 'allowMessageFromRule' => 'false'],
            
            //virtual_survey_pengumuman
            [['virtual_survey_pengumuman'], 'safe'],
            // [['virtual_survey_pengumuman'], 'each', 'rule' => ['required', 'on' => 'pembahasan', 'message' => '{attribute} tidak boleh kosong'], 'allowMessageFromRule' => 'false'],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        \Yii::$app->db->createCommand()->delete('peserta_survey_pembahasan', 'id_peserta = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_survey_pembahasan))
            foreach ($this->virtual_survey_pembahasan as $key => $value) {
                $psp = new PesertaSurveyPembahasan();
                $psp->id_peserta = $this->id;
                $psp->id_survey_pembahasan = $key;
                $psp->jawaban = $value;
                $psp->peserta_utama = 'Tidak';
                $psp->save();
            }
        
        \Yii::$app->db->createCommand()->delete('peserta_survey_pengumuman', 'id_peserta = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_survey_pengumuman))
            foreach ($this->virtual_survey_pengumuman as $key => $value) {
                $psp = new PesertaSurveyPengumuman();
                $psp->id_peserta = $this->id;
                $psp->id_survey_pengumuman = $key;
                $psp->jawaban = $value;
                $psp->peserta_utama = 'Tidak';
                $psp->save();
            }
    }

    public function afterFind()
    {
        parent::afterFind();
        
        $this->virtual_survey_pembahasan = \yii\helpers\ArrayHelper::map(\app_tryout\models\PesertaSurveyPembahasan::find()
            ->select(['sp.id', 'jawaban'])
            ->join('RIGHT JOIN', 'survey_pembahasan sp', 'sp.id = peserta_survey_pembahasan.id_survey_pembahasan AND id_peserta=' . $this->id)
            ->where(['peserta_survey_pembahasan.peserta_utama' => 'Tidak'])
            ->indexBy('id')
            ->asArray()
            ->all(),
        'id', 'jawaban');
        
        $this->virtual_survey_pengumuman = \yii\helpers\ArrayHelper::map(\app_tryout\models\PesertaSurveyPengumuman::find()
            ->select(['sp.id', 'jawaban'])
            ->join('RIGHT JOIN', 'survey_pengumuman sp', 'sp.id = peserta_survey_pengumuman.id_survey_pengumuman AND id_peserta=' . $this->id)
            ->where(['peserta_survey_pengumuman.peserta_utama' => 'Tidak'])
            ->indexBy('id')
            ->asArray()
            ->all(),
        'id', 'jawaban');
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_peserta' => 'Peserta',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'email' => 'Email',
            'handphone' => 'Handphone',
            'id_periode_jenis' => 'Jenis Tryout',
            'harga' => 'Harga',
            'periode_penjualan' => 'Periode Penjualan',
            'id_periode_kota' => 'Lokasi Tryout',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'line' => 'Line',
            'whatsapp' => 'Whatsapp',
            'status_survey_pembahasan' => 'Status Survey',
            'status_survey_pengumuman' => 'Status Survey',
            'kode_ptngo' => 'Kode PTN Go',
            'virtual_survey_pembahasan' => 'Jawaban',
            'virtual_survey_pengumuman' => 'Jawaban',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeserta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'id_peserta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeJenis()
    {
        return $this->hasOne(PeriodeJenis::className(), ['id' => 'id_periode_jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeKota()
    {
        return $this->hasOne(PeriodeKota::className(), ['id' => 'id_periode_kota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaSurveyPembahasans()
    {
        return $this->hasMany(PesertaSurveyPembahasan::className(), ['id_peserta' => 'id']);
    }
}
