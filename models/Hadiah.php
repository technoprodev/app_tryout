<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "hadiah".
 *
 * @property integer $id
 * @property integer $poin
 * @property string $nama
 *
 * @property PesertaHadiah[] $pesertaHadiahs
 */
class Hadiah extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'hadiah';
    }

    public function rules()
    {
        return [
            //id

            //poin
            [['poin'], 'required'],
            [['poin'], 'integer'],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poin' => 'Poin',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaHadiahs()
    {
        return $this->hasMany(PesertaHadiah::className(), ['id_hadiah' => 'id']);
    }
}
