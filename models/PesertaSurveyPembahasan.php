<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "peserta_survey_pembahasan".
 *
 * @property integer $id
 * @property integer $id_peserta
 * @property integer $id_survey_pembahasan
 * @property string $jawaban
 * @property string $peserta_utama
 *
 * @property Peserta $peserta
 * @property SurveyPembahasan $surveyPembahasan
 */
class PesertaSurveyPembahasan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'peserta_survey_pembahasan';
    }

    public function rules()
    {
        return [
            //id

            //id_peserta
            [['id_peserta'], 'required'],
            [['id_peserta'], 'integer'],
            // [['id_peserta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['id_peserta' => 'id']],

            //id_survey_pembahasan
            [['id_survey_pembahasan'], 'required'],
            [['id_survey_pembahasan'], 'integer'],
            [['id_survey_pembahasan'], 'exist', 'skipOnError' => true, 'targetClass' => SurveyPembahasan::className(), 'targetAttribute' => ['id_survey_pembahasan' => 'id']],

            //jawaban
            [['jawaban'], 'required'],
            [['jawaban'], 'string'],

            //peserta_utama
            [['peserta_utama'], 'required'],
            [['peserta_utama'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_peserta' => 'Id Peserta',
            'id_survey_pembahasan' => 'Id Survey Pembahasan',
            'jawaban' => 'Jawaban',
            'peserta_utama' => 'Peserta Utama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeserta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'id_peserta', 'peserta_utama' => 'Ya']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaTambahan()
    {
        return $this->hasOne(PesertaTambahan::className(), ['id' => 'id_peserta', 'peserta_utama' => 'Tidak']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyPembahasan()
    {
        return $this->hasOne(SurveyPembahasan::className(), ['id' => 'id_survey_pembahasan']);
    }
}
