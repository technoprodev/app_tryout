<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "promo".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property string $status
 * @property string $judul
 * @property string $link
 * @property string $picture
 * @property string $content
 *
 * @property Periode $periode
 */
class Promo extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_picture_upload;
    public $virtual_picture_download;

    public static function tableName()
    {
        return 'promo';
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //status
            [['status'], 'required'],
            [['status'], 'string'],

            //judul
            [['judul'], 'required'],
            [['judul'], 'string', 'max' => 256],

            //link
            [['link'], 'string', 'max' => 256],

            //picture
            [['picture'], 'string', 'max' => 256],

            //virtual_picture_download
            [['virtual_picture_download'], 'safe'],
            
            //virtual_picture_upload
            [['virtual_picture_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal upload 3MB'],

            //content
            [['content'], 'string'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_picture_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_picture_upload');
        if ($this->virtual_picture_upload) {
            $this->picture = $this->virtual_picture_upload->name;
            // $this->picture = $this->kode . '.' . $this->virtual_picture_upload->extension;
        }
        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_picture_upload) {
            if (!$insert) {
                $fileRoot = Yii::$app->params['configurations_file']['promo-picture']['alias_upload_root'];
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->picture;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->virtual_picture_upload) {
            $uploadRoot = Yii::$app->params['configurations_file']['promo-picture']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_picture_upload->saveAs($path . '/' . $this->picture);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = Yii::$app->params['configurations_file']['promo-picture']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->picture;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->picture) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['promo-picture']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_picture_download = $path . '/' . $this->picture;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Id Periode',
            'status' => 'Status',
            'judul' => 'Judul',
            'link' => 'Link',
            'picture' => 'Picture',
            'content' => 'Content',
            'virtual_picture_download' => 'File Umum',
            'virtual_picture_upload' => 'File Umum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }
}
