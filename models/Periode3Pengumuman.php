<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "periode3_pengumuman".
 *
 * @property integer $id
 * @property string $kode
 * @property string $jurusan
 * @property string $nama
 * @property string $kemampuan_penalaran_umum
 * @property string $pengetahuan_kuantitatif
 * @property string $pengetahuan_dan_pemahaman_umum
 * @property string $kemampuan_memahami_bacaan_dan_menulis
 * @property string $matematika_saintek
 * @property string $fisika
 * @property string $kimia
 * @property string $biologi
 * @property string $matematika_soshum
 * @property string $geografi
 * @property string $sejarah
 * @property string $sosiologi
 * @property string $ekonomi
 */
class Periode3Pengumuman extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'periode3_pengumuman';
    }

    public function rules()
    {
        return [
            //id

            //kode
            [['kode'], 'string', 'max' => 64],

            //jurusan
            [['jurusan'], 'string', 'max' => 64],

            //nama
            [['nama'], 'string', 'max' => 64],

            //kemampuan_penalaran_umum
            [['kemampuan_penalaran_umum'], 'string', 'max' => 64],

            //pengetahuan_kuantitatif
            [['pengetahuan_kuantitatif'], 'string', 'max' => 64],

            //pengetahuan_dan_pemahaman_umum
            [['pengetahuan_dan_pemahaman_umum'], 'string', 'max' => 64],

            //kemampuan_memahami_bacaan_dan_menulis
            [['kemampuan_memahami_bacaan_dan_menulis'], 'string', 'max' => 64],

            //matematika_saintek
            [['matematika_saintek'], 'string', 'max' => 64],

            //fisika
            [['fisika'], 'string', 'max' => 64],

            //kimia
            [['kimia'], 'string', 'max' => 64],

            //biologi
            [['biologi'], 'string', 'max' => 64],

            //matematika_soshum
            [['matematika_soshum'], 'string', 'max' => 64],

            //geografi
            [['geografi'], 'string', 'max' => 64],

            //sejarah
            [['sejarah'], 'string', 'max' => 64],

            //sosiologi
            [['sosiologi'], 'string', 'max' => 64],

            //ekonomi
            [['ekonomi'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'jurusan' => 'Jurusan',
            'nama' => 'Nama',
            'kemampuan_penalaran_umum' => 'Kemampuan Penalaran Umum',
            'pengetahuan_kuantitatif' => 'Pengetahuan Kuantitatif',
            'pengetahuan_dan_pemahaman_umum' => 'Pengetahuan Dan Pemahaman Umum',
            'kemampuan_memahami_bacaan_dan_menulis' => 'Kemampuan Memahami Bacaan Dan Menulis',
            'matematika_saintek' => 'Matematika Saintek',
            'fisika' => 'Fisika',
            'kimia' => 'Kimia',
            'biologi' => 'Biologi',
            'matematika_soshum' => 'Matematika Soshum',
            'geografi' => 'Geografi',
            'sejarah' => 'Sejarah',
            'sosiologi' => 'Sosiologi',
            'ekonomi' => 'Ekonomi',
        ];
    }
}
