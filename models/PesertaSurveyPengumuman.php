<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "peserta_survey_pengumuman".
 *
 * @property integer $id
 * @property integer $id_peserta
 * @property integer $id_survey_pengumuman
 * @property string $jawaban
 * @property string $peserta_utama
 *
 * @property Peserta $peserta
 * @property SurveyPengumuman $surveyPengumuman
 */
class PesertaSurveyPengumuman extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'peserta_survey_pengumuman';
    }

    public function rules()
    {
        return [
            //id

            //id_peserta
            [['id_peserta'], 'required'],
            [['id_peserta'], 'integer'],
            // [['id_peserta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['id_peserta' => 'id']],

            //id_survey_pengumuman
            [['id_survey_pengumuman'], 'required'],
            [['id_survey_pengumuman'], 'integer'],
            [['id_survey_pengumuman'], 'exist', 'skipOnError' => true, 'targetClass' => SurveyPengumuman::className(), 'targetAttribute' => ['id_survey_pengumuman' => 'id']],

            //jawaban
            [['jawaban'], 'required'],
            [['jawaban'], 'string'],

            //peserta_utama
            [['peserta_utama'], 'required'],
            [['peserta_utama'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_peserta' => 'Id Peserta',
            'id_survey_pengumuman' => 'Id Survey Pengumuman',
            'jawaban' => 'Jawaban',
            'peserta_utama' => 'Peserta Utama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeserta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'id_peserta', 'peserta_utama' => 'Ya']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaTambahan()
    {
        return $this->hasOne(PesertaTambahan::className(), ['id' => 'id_peserta', 'peserta_utama' => 'Tidak']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyPengumuman()
    {
        return $this->hasOne(SurveyPengumuman::className(), ['id' => 'id_survey_pengumuman']);
    }
}
