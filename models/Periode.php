<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "periode".
 *
 * @property integer $id
 * @property integer $kode
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $status
 * @property string $status_dashboard
 *
 * @property PeriodeJenis[] $periodeJenis
 * @property PeriodeKota[] $periodeKotas
 * @property PeriodeMetodePembayaran[] $periodeMetodePembayarans
 * @property Peserta[] $pesertas
 * @property Pic[] $pics
 * @property Promo[] $promos
 * @property ReferralAgent[] $referralAgents
 * @property SurveyPembahasan[] $surveyPembahasans
 * @property SurveyPengumuman[] $surveyPengumumen
 */
class Periode extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'periode';
    }

    public function rules()
    {
        return [
            //id

            //kode
            [['kode'], 'required'],
            [['kode'], 'integer'],

            //tanggal_mulai
            [['tanggal_mulai'], 'required'],
            [['tanggal_mulai'], 'safe'],

            //tanggal_selesai
            [['tanggal_selesai'], 'required'],
            [['tanggal_selesai'], 'safe'],

            //status
            [['status'], 'required'],
            [['status'], 'string'],

            //status_dashboard
            [['status_dashboard'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'status' => 'Status',
            'status_dashboard' => 'Status Dashboard',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeJenis()
    {
        return $this->hasMany(PeriodeJenis::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeKotas()
    {
        return $this->hasMany(PeriodeKota::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeMetodePembayarans()
    {
        return $this->hasMany(PeriodeMetodePembayaran::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPics()
    {
        return $this->hasMany(Pic::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromos()
    {
        return $this->hasMany(Promo::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferralAgents()
    {
        return $this->hasMany(ReferralAgent::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyPembahasans()
    {
        return $this->hasMany(SurveyPembahasan::className(), ['id_periode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyPengumumen()
    {
        return $this->hasMany(SurveyPengumuman::className(), ['id_periode' => 'id']);
    }

    public static function getPeriodeAktif()
    {
        if (($model = Periode::find()->where(['status' => 'Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Pendaftaran Tryout belum dibuka.');
        }
    }

    public static function getPeriodeNext()
    {
        $periodeAktif = Self::getPeriodeAktif();
        // periode selanjutnya adalah periode yang idnya tepat diatas id yang lagi aktif dan statusnya pending
        return (Self::find()->where(['status' => 'Pending', 'id' => ($periodeAktif->id + 1)])->one());
    }

    public static function getPeriodeDashboard()
    {
        if (($model = Periode::find()->where(['status_dashboard' => 'Tampilkan'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Harap ubah status dashboard menjadi Tampilkan melalui menu setting.');
        }
    }
}
