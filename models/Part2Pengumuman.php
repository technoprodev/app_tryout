<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "part2_pengumuman".
 *
 * @property integer $id
 * @property string $kode
 * @property string $Jurusan
 * @property string $Kemampuan Penalaran Umum
 * @property string $Pengetahuan Kuantitatif
 * @property string $Pengetahuan dan Pemahaman Umum
 * @property string $Kemampuan Memahami Bacaan dan Menulis
 * @property string $Matematika Saintek
 * @property string $Fisika
 * @property string $Kimia
 * @property string $Biologi
 * @property string $Matematika Soshum
 * @property string $Geografi
 * @property string $Sejarah
 * @property string $Sosiologi
 * @property string $Ekonomi
 */
class Part2Pengumuman extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'part2_pengumuman';
    }

    public function rules()
    {
        return [
            //id

            //kode
            [['kode'], 'string', 'max' => 64],

            //Jurusan
            [['Jurusan'], 'string', 'max' => 64],

            //Kemampuan Penalaran Umum
            [['Kemampuan Penalaran Umum'], 'string', 'max' => 64],

            //Pengetahuan Kuantitatif
            [['Pengetahuan Kuantitatif'], 'string', 'max' => 64],

            //Pengetahuan dan Pemahaman Umum
            [['Pengetahuan dan Pemahaman Umum'], 'string', 'max' => 64],

            //Kemampuan Memahami Bacaan dan Menulis
            [['Kemampuan Memahami Bacaan dan Menulis'], 'string', 'max' => 64],

            //Matematika Saintek
            [['Matematika Saintek'], 'string', 'max' => 64],

            //Fisika
            [['Fisika'], 'string', 'max' => 64],

            //Kimia
            [['Kimia'], 'string', 'max' => 64],

            //Biologi
            [['Biologi'], 'string', 'max' => 64],

            //Matematika Soshum
            [['Matematika Soshum'], 'string', 'max' => 64],

            //Geografi
            [['Geografi'], 'string', 'max' => 64],

            //Sejarah
            [['Sejarah'], 'string', 'max' => 64],

            //Sosiologi
            [['Sosiologi'], 'string', 'max' => 64],

            //Ekonomi
            [['Ekonomi'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'Jurusan' => 'Jurusan',
            'Kemampuan Penalaran Umum' => 'Kemampuan  Penalaran  Umum',
            'Pengetahuan Kuantitatif' => 'Pengetahuan  Kuantitatif',
            'Pengetahuan dan Pemahaman Umum' => 'Pengetahuan Dan  Pemahaman  Umum',
            'Kemampuan Memahami Bacaan dan Menulis' => 'Kemampuan  Memahami  Bacaan Dan  Menulis',
            'Matematika Saintek' => 'Matematika  Saintek',
            'Fisika' => 'Fisika',
            'Kimia' => 'Kimia',
            'Biologi' => 'Biologi',
            'Matematika Soshum' => 'Matematika  Soshum',
            'Geografi' => 'Geografi',
            'Sejarah' => 'Sejarah',
            'Sosiologi' => 'Sosiologi',
            'Ekonomi' => 'Ekonomi',
        ];
    }
}
