<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "periode3_tokped".
 *
 * @property integer $id
 * @property string $kode
 * @property integer $id_transaksi
 * @property integer $harga
 *
 * @property Transaksi $transaksi
 */
class Periode3Tokped extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'periode3_tokped';
    }

    public function rules()
    {
        return [
            //id

            //kode
            [['kode'], 'required'],
            [['kode'], 'string', 'max' => 256],
            [['kode'], 'unique'],

            //id_transaksi
            [['id_transaksi'], 'integer'],
            [['id_transaksi'], 'unique'],
            [['id_transaksi'], 'exist', 'skipOnError' => true, 'targetClass' => Transaksi::className(), 'targetAttribute' => ['id_transaksi' => 'id']],

            //harga
            [['harga'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'id_transaksi' => 'Id Transaksi',
            'harga' => 'Harga',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksi()
    {
        return $this->hasOne(Transaksi::className(), ['id' => 'id_transaksi']);
    }
}
