<?php
namespace app_tryout\models;

use Yii;
use yii\base\Model;
use app_tryout\models\Pic;

class LoginPic extends Model
{
    public $kode;
    public $username;
    public $rememberMe = true;

    public $pic;

    public function rules()
    {
        return [
            ['kode', 'required', 'on' => 'using-kode'],
            
            ['username', 'required'],
            ['username', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'username' => 'Password',
            'rememberMe' => 'Remember Me',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if ($pic = $this->getPic()) {
            if (!$pic->validatePassword($this->username)) {
                $this->addError($attribute, 'Incorrect username.');
            }
        } else {
            switch ($this->scenario) {
                case 'using-kode':
                    $this->addError('kode', 'Kode is not exists.');
                    break;
                default:
                    $this->addError('kode', 'Kode is not exists.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->userPic->login($this->getPic(), $this->rememberMe ? Yii::$app->params['user.passwordResetTokenExpire'] * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    protected function getPic()
    {
        if ($this->pic === null) {
            switch ($this->scenario) {
                case 'using-kode':
                    $this->pic = Pic::findByKode($this->kode);
                    break;
                default:
                    $this->pic = Pic::findByKode($this->kode);
            }
        }

        return $this->pic;
    }
}