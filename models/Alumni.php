<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "alumni".
 *
 * @property integer $id
 * @property string $nama
 * @property string $poto
 * @property string $universitas
 * @property string $jurusan
 * @property string $angkatan
 */
class Alumni extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_poto_upload;
    public $virtual_poto_download;

    public static function tableName()
    {
        return 'alumni';
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],

            //poto
            [['poto'], 'required'],
            [['poto'], 'string', 'max' => 256],

            //universitas
            [['universitas'], 'string', 'max' => 256],

            //jurusan
            [['jurusan'], 'string', 'max' => 256],

            //angkatan
            [['angkatan'], 'safe'],
            
            //virtual_poto_download
            [['virtual_poto_download'], 'safe'],
            
            //virtual_poto_upload
            [['virtual_poto_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal upload 3MB'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_poto_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_poto_upload');
        if ($this->virtual_poto_upload) {
            $this->poto = $this->virtual_poto_upload->name;
            // $this->poto = $this->kode . '.' . $this->virtual_poto_upload->extension;
        }
        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_poto_upload) {
            if (!$insert) {
                $fileRoot = '@upload-alumni-poto';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->poto;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->virtual_poto_upload) {
            $uploadRoot = '@upload-alumni-poto';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_poto_upload->saveAs($path . '/' . $this->poto);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $fileRoot = '@upload-alumni-poto';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->poto;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();

        if($this->poto) {
            $downloadBaseUrl = '@download-alumni-poto';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_poto_download = $path . '/' . $this->poto;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'poto' => 'Poto',
            'universitas' => 'Universitas',
            'jurusan' => 'Jurusan',
            'angkatan' => 'Angkatan',
        ];
    }
}
