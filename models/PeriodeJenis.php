<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "periode_jenis".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property string $nama
 * @property string $periode_penjualan
 * @property string $status
 * @property string $waktu_berakhir
 * @property integer $harga_1_tiket
 * @property integer $harga_2_tiket
 * @property integer $harga_3_tiket
 * @property integer $harga_4_tiket
 * @property integer $harga_5_tiket
 * @property integer $harga_6_tiket
 * @property integer $harga_7_tiket
 * @property integer $harga_8_tiket
 * @property integer $harga_9_tiket
 * @property integer $harga_10_tiket
 *
 * @property Periode $periode
 * @property Peserta[] $pesertas
 * @property PesertaTambahan[] $pesertaTambahans
 */
class PeriodeJenis extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'periode_jenis';
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 32],

            //periode_penjualan
            [['periode_penjualan'], 'required'],
            [['periode_penjualan'], 'string', 'max' => 256],

            //status
            [['status'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['status'], 'string'],

            //waktu_berakhir
            [['waktu_berakhir'], 'string'],

            //harga_1_tiket
            [['harga_1_tiket'], 'required'],
            [['harga_1_tiket'], 'integer'],

            //harga_2_tiket
            [['harga_2_tiket'], 'integer'],

            //harga_3_tiket
            [['harga_3_tiket'], 'integer'],

            //harga_4_tiket
            [['harga_4_tiket'], 'integer'],

            //harga_5_tiket
            [['harga_5_tiket'], 'integer'],

            //harga_6_tiket
            [['harga_6_tiket'], 'integer'],

            //harga_7_tiket
            [['harga_7_tiket'], 'integer'],

            //harga_8_tiket
            [['harga_8_tiket'], 'integer'],

            //harga_9_tiket
            [['harga_9_tiket'], 'integer'],

            //harga_10_tiket
            [['harga_10_tiket'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Id Periode',
            'nama' => 'Nama',
            'periode_penjualan' => 'Periode Penjualan',
            'status' => 'Status',
            'waktu_berakhir' => 'Waktu Berakhir',
            'harga_1_tiket' => 'Harga 1 Tiket',
            'harga_2_tiket' => 'Harga 2 Tiket',
            'harga_3_tiket' => 'Harga 3 Tiket',
            'harga_4_tiket' => 'Harga 4 Tiket',
            'harga_5_tiket' => 'Harga 5 Tiket',
            'harga_6_tiket' => 'Harga 6 Tiket',
            'harga_7_tiket' => 'Harga 7 Tiket',
            'harga_8_tiket' => 'Harga 8 Tiket',
            'harga_9_tiket' => 'Harga 9 Tiket',
            'harga_10_tiket' => 'Harga 10 Tiket',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['id_periode_jenis' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaTambahans()
    {
        return $this->hasMany(PesertaTambahan::className(), ['id_periode_jenis' => 'id']);
    }
}
