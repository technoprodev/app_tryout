<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "pic_umum".
 *
 * @property integer $id
 * @property string $note_umum
 * @property string $file_umum
 */
class PicUmum extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_file_umum_upload;
    public $virtual_file_umum_download;

    public static function tableName()
    {
        return 'pic_umum';
    }

    public function rules()
    {
        return [
            //id

            //note_umum
            [['note_umum'], 'string'],

            //file_umum
            [['file_umum'], 'string'],
            
            //virtual_file_umum_download
            [['virtual_file_umum_download'], 'safe'],
            
            //virtual_file_umum_upload
            [['virtual_file_umum_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal upload 3MB'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_file_umum_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_file_umum_upload');
        if ($this->virtual_file_umum_upload) {
            $this->file_umum = $this->virtual_file_umum_upload->name;
            // $this->file_umum = $this->kode . '.' . $this->virtual_file_umum_upload->extension;
        }
        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_file_umum_upload) {
            if (!$insert) {
                $fileRoot = '@upload-pic_umum-file_umum';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->file_umum;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->virtual_file_umum_upload) {
            $uploadRoot = '@upload-pic_umum-file_umum';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_file_umum_upload->saveAs($path . '/' . $this->file_umum);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = '@upload-pic_umum-file_umum';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->file_umum;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->file_umum) {
            $downloadBaseUrl = '@download-pic_umum-file_umum';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_file_umum_download = $path . '/' . $this->file_umum;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'note_umum' => 'Note Umum',
            'file_umum' => 'File Umum',
            'virtual_file_umum_download' => 'File Umum',
            'virtual_file_umum_upload' => 'File Umum',
        ];
    }

    public static function getModel()
    {
        if (($model = self::find()->where(['id' => 1])->one()) == null) {
            $model = new PicUmum();
        }
        return $model;
    }
}
