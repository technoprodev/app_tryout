<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "misi".
 *
 * @property integer $id
 * @property string $judul
 * @property string $keterangan
 * @property string $status_aktif
 * @property string $dari_tanggal
 * @property string $sampai_tanggal
 */
class Misi extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'misi';
    }

    public function rules()
    {
        return [
            //id

            //judul
            [['judul'], 'required'],
            [['judul'], 'string', 'max' => 256],

            //keterangan
            [['keterangan'], 'string'],

            //status_aktif
            [['status_aktif'], 'string'],

            //dari_tanggal
            [['dari_tanggal'], 'required'],
            [['dari_tanggal'], 'safe'],

            //sampai_tanggal
            [['sampai_tanggal'], 'required'],
            [['sampai_tanggal'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'keterangan' => 'Keterangan',
            'status_aktif' => 'Status Aktif',
            'dari_tanggal' => 'Dari Tanggal',
            'sampai_tanggal' => 'Sampai Tanggal',
        ];
    }
}
