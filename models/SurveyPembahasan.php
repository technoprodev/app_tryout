<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "survey_pembahasan".
 *
 * @property integer $id
 * @property string $pertanyaan
 * @property string $tipe_pertanyaan
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $e
 *
 * @property PesertaSurveyPembahasan[] $pesertaSurveyPembahasans
 */
class SurveyPembahasan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'survey_pembahasan';
    }

    public function rules()
    {
        return [
            //id

            //pertanyaan
            [['pertanyaan'], 'required'],
            [['pertanyaan'], 'string'],

            //tipe_pertanyaan
            [['tipe_pertanyaan'], 'required'],
            [['tipe_pertanyaan'], 'string'],

            //a
            [['a'], 'string'],

            //b
            [['b'], 'string'],

            //c
            [['c'], 'string'],

            //d
            [['d'], 'string'],

            //e
            [['e'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pertanyaan' => 'Pertanyaan',
            'tipe_pertanyaan' => 'Tipe Pertanyaan',
            'a' => 'A',
            'b' => 'B',
            'c' => 'C',
            'd' => 'D',
            'e' => 'E',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaSurveyPembahasans()
    {
        return $this->hasMany(PesertaSurveyPembahasan::className(), ['id_survey_pembahasan' => 'id']);
    }
}
