<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "jurusan".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property Peserta[] $pesertas
 */
class Jurusan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jurusan';
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['id_jurusan' => 'id']);
    }
}
