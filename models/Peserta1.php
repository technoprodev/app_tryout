<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "peserta".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property integer $kode
 * @property string $status_bayar
 * @property string $created_at
 * @property string $updated_at
 * @property string $confirm_request_at
 * @property string $confirm_at
 * @property string $status_aktif
 * @property string $nonaktif_at
 * @property string $status_duta
 * @property string $status_klaim
 * @property integer $id_duta
 * @property string $nama
 * @property string $email
 * @property string $handphone
 * @property integer $id_periode_jenis
 * @property integer $harga
 * @property string $periode_penjualan
 * @property string $id_request_kota
 * @property integer $id_periode_kota
 * @property string $jenis_kelamin
 * @property string $sekolah
 * @property string $alamat
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $line
 * @property string $whatsapp
 * @property integer $id_sumber
 * @property integer $id_jurusan
 * @property integer $jumlah_tiket
 * @property integer $tagihan
 * @property integer $id_periode_metode_pembayaran
 * @property string $tanggal_pembayaran
 * @property string $pembayaran_atas_nama
 * @property string $bukti_pembayaran
 * @property string $informasi_pembayaran
 * @property string $catatan
 * @property string $keterangan_gratis
 * @property string $kode_referral
 * @property string $status_survey_pembahasan
 * @property string $status_survey_pengumuman
 * @property integer $resend_pendaftaran_1
 * @property string $resend_pendaftaran_1_date
 * @property integer $resend_pendaftaran_2
 * @property string $resend_pendaftaran_2_date
 * @property string $kode_ptngo
 *
 * @property Periode $periode
 * @property PeriodeJenis $periodeJenis
 * @property PeriodeKota $periodeKota
 * @property Sumber $sumber
 * @property Jurusan $jurusan
 * @property PeriodeMetodePembayaran $periodeMetodePembayaran
 * @property Peserta $duta
 * @property Peserta[] $pesertas
 * @property ReferralAgent $referralAgent
 * @property Regencies $requestKota
 * @property PesertaHadiah[] $pesertaHadiahs
 * @property PesertaSurveyPembahasan[] $pesertaSurveyPembahasans
 * @property PesertaTambahan[] $pesertaTambahans
 */
class Peserta extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_hadiah = [];
    public $virtual_survey_pembahasan = [];
    public $virtual_survey_pengumuman = [];
    public $virtual_poin = 0;
    public $virtual_poin_klaim = 0;
    public $virtual_bukti_pembayaran_upload;
    public $virtual_bukti_pembayaran_download;

    public static function tableName()
    {
        return 'peserta';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //kode
            [['kode'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['kode'], 'integer', 'message' => '{attribute} harus angka'],

            //status_bayar
            [['status_bayar'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['status_bayar'], 'string'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //confirm_request_at
            [['confirm_request_at'], 'safe'],

            //confirm_at
            [['confirm_at'], 'safe'],

            //status_aktif
            [['status_aktif'], 'string'],

            //nonaktif_at
            [['nonaktif_at'], 'safe'],

            //status_duta
            [['status_duta'], 'string'],

            //status_klaim
            [['status_klaim'], 'string'],

            //id_duta
            [['id_duta'], 'integer'],
            [['id_duta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['id_duta' => 'id']],

            //nama
            [['nama'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['nama'], 'string', 'max' => 256],

            //email
            [['email'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['email'], 'string', 'max' => 256],
            [['email'], 'email', 'message' => '{attribute} tidak valid'],
            [['email'], 'match', 'pattern' => '/^[\w.+\-]+@(aol.com|gmail.com|hotmail.com|live.com|outlook.com|[\w.+\-]+sch.id|yahoo.co.id|yahoo.com|ymail.com)$/', 'message' => '{attribute} yang diperbolehkan hanya aol.com, gmail.com, hotmail.com, live.com, outlook.com, sch.id, co.id, yahoo.com, ymail.com'],

            //handphone
            [['handphone'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['handphone'], 'number', 'message' => '{attribute} harus angka'],
            [['handphone'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['handphone'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //id_periode_jenis
            [['id_periode_jenis'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_periode_jenis'], 'integer'],
            [['id_periode_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeJenis::className(), 'targetAttribute' => ['id_periode_jenis' => 'id']],

            //harga
            [['harga'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['harga'], 'integer'],

            //periode_penjualan
            [['periode_penjualan'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['periode_penjualan'], 'string', 'max' => 256],

            //id_request_kota
            [['id_request_kota'], 'string', 'max' => 4],
            [['id_request_kota'], 'exist', 'skipOnError' => true, 'targetClass' => \technosmart\modules\location\models\Regencies::className(), 'targetAttribute' => ['id_request_kota' => 'id']],

            //id_periode_kota
            [['id_periode_kota'], 'required', 'message' => '{attribute} tidak boleh kosong', 'on' => 'default'],
            [['id_periode_kota'], 'integer'],
            [['id_periode_kota'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeKota::className(), 'targetAttribute' => ['id_periode_kota' => 'id']],

            //jenis_kelamin
            [['jenis_kelamin'], 'string'],

            //sekolah
            [['sekolah'], 'string', 'max' => 256],

            //alamat
            [['alamat'], 'string', 'max' => 256],

            //facebook
            [['facebook'], 'string', 'max' => 256],

            //twitter
            [['twitter'], 'string', 'max' => 256],

            //instagram
            [['instagram'], 'string', 'max' => 256],

            //line
            [['line'], 'string', 'max' => 256],

            //whatsapp
            [['whatsapp'], 'number', 'message' => '{attribute} harus angka'],
            [['whatsapp'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['whatsapp'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //id_sumber
            [['id_sumber'], 'integer'],
            [['id_sumber'], 'exist', 'skipOnError' => true, 'targetClass' => Sumber::className(), 'targetAttribute' => ['id_sumber' => 'id']],

            //id_jurusan
            [['id_jurusan'], 'integer'],
            [['id_jurusan'], 'exist', 'skipOnError' => true, 'targetClass' => Jurusan::className(), 'targetAttribute' => ['id_jurusan' => 'id']],

            //jumlah_tiket
            [['jumlah_tiket'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['jumlah_tiket'], 'integer'],

            //tagihan
            [['tagihan'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['tagihan'], 'integer'],

            //id_periode_metode_pembayaran
            [['id_periode_metode_pembayaran'], 'integer'],
            [['id_periode_metode_pembayaran'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeMetodePembayaran::className(), 'targetAttribute' => ['id_periode_metode_pembayaran' => 'id']],

            //tanggal_pembayaran
            [['tanggal_pembayaran'], 'safe'],

            //pembayaran_atas_nama
            [['pembayaran_atas_nama'], 'string', 'max' => 256],

            //bukti_pembayaran
            [['bukti_pembayaran'], 'string', 'max' => 256],

            //informasi_pembayaran
            [['informasi_pembayaran'], 'string', 'max' => 256],

            //catatan
            [['catatan'], 'string', 'max' => 256],

            //keterangan_gratis
            [['keterangan_gratis'], 'string', 'max' => 256],

            //kode_referral
            [['kode_referral'], 'string', 'max' => 32],

            //status_survey_pembahasan
            [['status_survey_pembahasan'], 'string'],

            //status_survey_pengumuman
            [['status_survey_pengumuman'], 'string'],

            //resend_pendaftaran_1
            [['resend_pendaftaran_1'], 'integer'],

            //resend_pendaftaran_1_date
            [['resend_pendaftaran_1_date'], 'safe'],

            //resend_pendaftaran_2
            [['resend_pendaftaran_2'], 'integer'],

            //resend_pendaftaran_2_date
            [['resend_pendaftaran_2_date'], 'safe'],

            //kode_ptngo
            [['kode_ptngo'], 'string'],
            
            //virtual_hadiah
            [['virtual_hadiah'], function ($attribute, $params) {
                if ($this->virtual_poin < $this->virtual_poin_klaim) {
                    $this->addError($attribute, 'Hadiah yang kamu klaim melebihi poin yang kamu punya. Harap tambah poin ambassador kamu atau kurangi poin hadiah yang kamu klaim.');
                }
            }],
            
            //virtual_survey_pembahasan
            [['virtual_survey_pembahasan'], 'safe'],
            // [['virtual_survey_pembahasan'], 'each', 'rule' => ['required', 'message' => '{attribute} tidak boleh kosong'], 'allowMessageFromRule' => 'false', 'on' => 'pembahasan'],
            
            //virtual_survey_pengumuman
            [['virtual_survey_pengumuman'], 'safe'],
            // [['virtual_survey_pengumuman'], 'each', 'rule' => ['required', 'message' => '{attribute} tidak boleh kosong'], 'allowMessageFromRule' => 'false', 'on' => 'pengumuman'],
            
            //virtual_bukti_pembayaran_download
            [['virtual_bukti_pembayaran_download'], 'safe'],
            
            //virtual_bukti_pembayaran_upload
            [['virtual_bukti_pembayaran_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal screenshot bukti bayar adalah 3MB'],

            //konfirmasi
            [['id_periode_metode_pembayaran', 'tanggal_pembayaran', 'pembayaran_atas_nama', 'virtual_bukti_pembayaran_upload'], 'required', 'message' => '{attribute} tidak boleh kosong', 'on' => 'konfirmasi'],

            //custom
            [['id_request_kota'], 'required', 'on' => 'pendaftaran-awal', 'message' => '{attribute} tidak boleh kosong'],

            [['jenis_kelamin', 'sekolah', 'id_sumber', 'alamat'], 'required', 'message' => '{attribute} tidak boleh kosong'],

            [['kode_referral'], 'required', 'on' => 'referral', 'message' => '{attribute} tidak boleh kosong'],
            [['kode_referral'], 'in', 'range' => [
                'b10kagibdg48',
                'b10kcandsby54',
                'b10kceljkt 51',
                'b10kcherygy52',
                'b10kfatibgr56',
                'b10kfebrpdg57',
                'b10kidasmg53',
                'b10kima mdn49',
                'b10knadibks55',
                'b10kwidiaplmb50',
                'picajibks22',
                'picakbarjktim03',
                'picanjartlgag25',
                'picauliamks12',
                'picdecitsk31',
                'picdikimdn44',
                'picdinaplmb09',
                'picesterjambi18',
                'picfitrilamo29',
                'picgayuhkrw02',
                'picginamjlk30',
                'picgunturbdgg24',
                'picimambdg23',
                'picinnasbg01',
                'picmayatgr42',
                'picmedaserang40',
                'picmerikabtgr35',
                'picmilanijksel37',
                'picnadaultgsel04',
                'picniasmg11',
                'picninagrt38',
                'picnurulckr27',
                'picpauljyp41',
                'picrefibny05',
                'picrestibkt26',
                'picsabanaach07',
                'picsendrasby10',
                'picshintacrb43',
                'picsilvadepk32',
                'picsscpntk08',
                'picsucisido33',
                'picteremndo06',
                'picugikpsruan45',
                'picwahyupkb39',
                'pgyzafira58',
                'pgyainun59',
                'pgychoirul60',
                'picgeraldpic61',
                'picevimlg62',
                'b10kfarhanmlg63',
                'b10kdestidpk64',
                'b10kmardimks65',
                'SNStiket66',
                'picanipklg15',
                'picsasibjngr16',
                'picdianbrbs21',
                'aoigesta67',
                'pgysherina68',
                'pgyzaky69',
                'pgysyaiful70',
                'pgyjaya71',
                'pgycalvin72',
                'pgyfatah73',
                'pichendrawnsbo13',
                'picsakuratmgg14',
                'picmaretapurb19',
                'picmelindakltn20',
                'picviapml46',
                'piciqbalmgl74',
                'piczamjmbr28',
            ], 'on' => 'referral', 'message' => '{attribute} tidak valid'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_bukti_pembayaran_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_pembayaran_upload');
        if ($this->virtual_bukti_pembayaran_upload) {
            // $this->bukti_pembayaran = $this->virtual_bukti_pembayaran_upload->name;
            $this->bukti_pembayaran = $this->kode . '.' . $this->virtual_bukti_pembayaran_upload->extension;
        }

        $this->virtual_poin_klaim = (new \yii\db\Query())
            ->select('sum(poin)')
            ->from('hadiah h')
            ->where(['h.id' => $this->virtual_hadiah])
            ->scalar();
        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_bukti_pembayaran_upload) {
            if (!$insert) {
                $fileRoot = '@upload-peserta-bukti_pembayaran';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_pembayaran;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        \Yii::$app->db->createCommand()->delete('peserta_hadiah', 'id_peserta = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_hadiah))
            foreach ($this->virtual_hadiah as $key => $val) {
                $ph = new PesertaHadiah();
                $ph->id_peserta = $this->id;
                $ph->id_hadiah = $val;
                $ph->save();
            }
        
        \Yii::$app->db->createCommand()->delete('peserta_survey_pembahasan', 'id_peserta = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_survey_pembahasan))
            foreach ($this->virtual_survey_pembahasan as $key => $value) {
                $psp = new PesertaSurveyPembahasan();
                $psp->id_peserta = $this->id;
                $psp->id_survey_pembahasan = $key;
                $psp->jawaban = $value;
                $psp->peserta_utama = 'Ya';
                $psp->save();
            }
        
        \Yii::$app->db->createCommand()->delete('peserta_survey_pengumuman', 'id_peserta = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_survey_pengumuman))
            foreach ($this->virtual_survey_pengumuman as $key => $value) {
                $psp = new PesertaSurveyPengumuman();
                $psp->id_peserta = $this->id;
                $psp->id_survey_pengumuman = $key;
                $psp->jawaban = $value;
                $psp->peserta_utama = 'Ya';
                $psp->save();
            }
        
        if ($this->virtual_bukti_pembayaran_upload) {
            $uploadRoot = '@upload-peserta-bukti_pembayaran';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_pembayaran_upload->saveAs($path . '/' . $this->bukti_pembayaran);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = '@upload-peserta-bukti_pembayaran';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_pembayaran;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        foreach ($this->getPesertaHadiahs()->asArray()->all() as $key => $value) {
            $this->virtual_hadiah[] = $value['id_hadiah'];
        }

        $jumlahTiket = 0;
        foreach ($this->pesertas as $key => $peserta) {
            if ($peserta->status_bayar == 'Sudah Bayar' && $peserta->status_aktif == 'Aktif') {
                $jumlahTiket++;
            }
            foreach ($peserta->pesertaTambahans as $key => $pesertaTambahan) {
                if ($peserta->status_bayar == 'Sudah Bayar' && $peserta->status_aktif == 'Aktif') {
                    $jumlahTiket++;
                }
            }
        }
        $this->virtual_poin = $jumlahTiket * 15;

        foreach ($this->virtual_hadiah as $key => $value) {
            $this->virtual_poin_klaim += (new \yii\db\Query())
                ->select('sum(poin)')
                ->from('hadiah h')
                ->where(['h.id' => $value])
                ->scalar();
        }
        
        $this->virtual_survey_pembahasan = \yii\helpers\ArrayHelper::map(\app_tryout\models\PesertaSurveyPembahasan::find()
            ->select(['sp.id', 'jawaban'])
            ->join('RIGHT JOIN', 'survey_pembahasan sp', 'sp.id = peserta_survey_pembahasan.id_survey_pembahasan AND id_peserta=' . $this->id)
            ->where(['peserta_survey_pembahasan.peserta_utama' => 'Ya'])
            ->indexBy('id')
            ->asArray()
            ->all(),
        'id', 'jawaban');
        // ddx(\app_tryout\models\PesertaSurveyPembahasan::find()->select(['sp.id', 'jawaban'])->join('RIGHT JOIN', 'survey_pembahasan sp', 'sp.id = peserta_survey_pembahasan.id_survey_pembahasan')->indexBy('id')->where(['id_peserta' => $this->id])->createCommand()->sql);
        // ddx($this);
        // ddx($this->virtual_survey_pembahasan);
        
        $this->virtual_survey_pengumuman = \yii\helpers\ArrayHelper::map(\app_tryout\models\PesertaSurveyPengumuman::find()
            ->select(['sp.id', 'jawaban'])
            ->join('RIGHT JOIN', 'survey_pengumuman sp', 'sp.id = peserta_survey_pengumuman.id_survey_pengumuman AND id_peserta=' . $this->id)
            ->where(['peserta_survey_pengumuman.peserta_utama' => 'Ya'])
            ->indexBy('id')
            ->asArray()
            ->all(),
        'id', 'jawaban');
        // ddx(\app_tryout\models\PesertaSurveyPengumuman::find()->select(['sp.id', 'jawaban'])->join('RIGHT JOIN', 'survey_pengumuman sp', 'sp.id = peserta_survey_pengumuman.id_survey_pengumuman')->indexBy('id')->where(['id_peserta' => $this->id])->createCommand()->sql);
        // ddx($this);
        // ddx($this->virtual_survey_pengumuman);

        if($this->bukti_pembayaran) {
            $downloadBaseUrl = '@download-peserta-bukti_pembayaran';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_pembayaran_download = $path . '/' . $this->bukti_pembayaran;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Periode',
            'kode' => 'Kode',
            'status_bayar' => 'Status Bayar',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'confirm_request_at' => 'Confirmation Requested At',
            'confirm_at' => 'Confirmed At',
            'status_aktif' => 'Status Aktif',
            'nonaktif_at' => 'Nonaktif At',
            'status_duta' => 'Status Duta',
            'status_klaim' => 'Status Klaim',
            'id_duta' => 'Duta',
            'nama' => 'Nama',
            'email' => 'Email',
            'handphone' => 'Handphone',
            'id_periode_jenis' => 'Jenis Tryout',
            'harga' => 'Harga',
            'periode_penjualan' => 'Periode Penjualan',
            'id_request_kota' => 'Request Kota',
            'id_periode_kota' => 'Lokasi Tryout',
            'jenis_kelamin' => 'Jenis Kelamin',
            'sekolah' => 'Sekolah',
            'alamat' => 'Alamat',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'line' => 'Line',
            'whatsapp' => 'Whatsapp',
            'id_sumber' => 'Sumber',
            'id_jurusan' => 'Jurusan',
            'jumlah_tiket' => 'Jumlah Tiket',
            'tagihan' => 'Tagihan',
            'id_periode_metode_pembayaran' => 'Periode Metode Pembayaran',
            'tanggal_pembayaran' => 'Tanggal Pembayaran',
            'pembayaran_atas_nama' => 'Pembayaran Atas Nama',
            'bukti_pembayaran' => 'Bukti Pembayaran',
            'informasi_pembayaran' => 'Informasi Pembayaran',
            'catatan' => 'Catatan',
            'keterangan_gratis' => 'Keterangan Gratis',
            'kode_referral' => 'Kode Referral',
            'status_survey_pembahasan' => 'Status Survey',
            'status_survey_pengumuman' => 'Status Survey',
            'resend_pendaftaran_1' => 'Resend Pendaftaran 1',
            'resend_pendaftaran_1_date' => 'Resend Pendaftaran 1 Date',
            'resend_pendaftaran_2' => 'Resend Pendaftaran 2',
            'resend_pendaftaran_2_date' => 'Resend Pendaftaran 2 Date',
            'kode_ptngo' => 'Kode PTN Go',
            'virtual_hadiah' => 'Hadiah',
            'virtual_survey_pembahasan' => 'Jawaban',
            'virtual_survey_pengumuman' => 'Jawaban',
            'virtual_bukti_pembayaran_download' => 'Bukti Pembayaran',
            'virtual_bukti_pembayaran_upload' => 'Bukti Pembayaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeJenis()
    {
        return $this->hasOne(PeriodeJenis::className(), ['id' => 'id_periode_jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeKota()
    {
        return $this->hasOne(PeriodeKota::className(), ['id' => 'id_periode_kota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSumber()
    {
        return $this->hasOne(Sumber::className(), ['id' => 'id_sumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJurusan()
    {
        return $this->hasOne(Jurusan::className(), ['id' => 'id_jurusan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeMetodePembayaran()
    {
        return $this->hasOne(PeriodeMetodePembayaran::className(), ['id' => 'id_periode_metode_pembayaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDuta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'id_duta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['id_duta' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferralAgent()
    {
        return $this->hasOne(ReferralAgent::className(), ['id' => 'kode_referral_agent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestKota()
    {
        return $this->hasOne(\technosmart\modules\location\models\Regencies::className(), ['id' => 'id_request_kota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaHadiahs()
    {
        return $this->hasMany(PesertaHadiah::className(), ['id_peserta' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaSurveyPembahasans()
    {
        return $this->hasMany(PesertaSurveyPembahasan::className(), ['id_peserta' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaTambahans()
    {
        return $this->hasMany(PesertaTambahan::className(), ['id_peserta' => 'id']);
    }
}
