<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "referral_agent".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property string $kode
 * @property string $handphone
 * @property string $status
 * @property string $nama
 * @property integer $fee
 * @property string $program
 * @property string $sekolah
 * @property string $id_regencies
 * @property string $email
 * @property string $nomor_identitas
 * @property string $alamat_lengkap
 * @property string $catatan
 *
 * @property Regencies $regencies
 * @property Periode $periode
 * @property Transaksi[] $transaksis
 */
class ReferralAgent extends \technosmart\yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName()
    {
        return 'referral_agent';
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //kode
            [['kode'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['kode'], 'string', 'max' => 16],

            //handphone
            [['handphone'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['handphone'], 'number', 'message' => '{attribute} harus angka'],
            [['handphone'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['handphone'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //status
            [['status'], 'string'],

            //nama
            [['nama'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['nama'], 'string', 'max' => 256],

            //fee
            [['fee'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['fee'], 'integer', 'message' => '{attribute} harus angka'],

            //program
            [['program'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['program'], 'string', 'max' => 256],

            //sekolah
            [['sekolah'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['sekolah'], 'string', 'max' => 256],

            //id_regencies
            [['id_regencies'], 'string', 'max' => 4],
            [['id_regencies'], 'exist', 'skipOnError' => true, 'targetClass' => \technosmart\modules\location\models\Regencies::className(), 'targetAttribute' => ['id_regencies' => 'id']],

            //email
            [['email'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['email'], 'email', 'message' => '{attribute} tidak valid'],
            [['email'], 'string', 'max' => 256],

            //nomor_identitas
            [['nomor_identitas'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['nomor_identitas'], 'string', 'max' => 256],

            //alamat_lengkap
            [['alamat_lengkap'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['alamat_lengkap'], 'string'],

            //catatan
            [['catatan'], 'string'],
        ];
    }

    public static function findByKode($kode)
    {
        return static::find()
            ->join('INNER JOIN', 'periode p', 'p.id = referral_agent.id_periode')
            ->where('referral_agent.kode = :kode', [':kode' => $kode])
            ->andWhere(['referral_agent.status' => 'Sedang Aktif'])
            ->andWhere(['p.id' => (Periode::getPeriodeAktif())->id])
            ->one();
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id/*, 'status' => 'Aktif'*/]);
    }

    /**
     * Login secara API based
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['password' => $token/*, 'status' => 'Aktif'*/]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->handphone;
    }

    /**
     * Membandingkan Authkey existing dengan Authkey yang ada di browser
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->handphone === $password;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Id Periode',
            'kode' => 'Kode',
            'handphone' => 'Handphone',
            'status' => 'Status',
            'nama' => 'Nama',
            'fee' => 'Fee',
            'program' => 'Program',
            'sekolah' => 'Sekolah',
            'id_regencies' => 'Id Regencies',
            'email' => 'Email',
            'nomor_identitas' => 'Nomor Identitas',
            'alamat_lengkap' => 'Alamat Lengkap',
            'catatan' => 'Catatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegencies()
    {
        return $this->hasOne(\technosmart\modules\location\models\Regencies::className(), ['id' => 'id_regencies']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksis()
    {
        return $this->hasMany(Transaksi::className(), ['kode_referral_agent' => 'id']);
    }
}
