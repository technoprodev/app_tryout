<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "peserta".
 *
 * @property integer $id
 * @property integer $id_transaksi
 * @property string $peserta_utama
 * @property string $username
 * @property string $password
 * @property string $kode
 * @property string $nama
 * @property string $email
 * @property string $handphone
 * @property integer $id_periode_jenis
 * @property integer $harga
 * @property string $periode_penjualan
 * @property integer $id_periode_kota
 * @property string $jenis_kelamin
 * @property string $sekolah
 * @property string $id_kota
 * @property string $alamat
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $line
 * @property string $whatsapp
 * @property integer $id_jurusan
 * @property string $status_survey_pembahasan
 * @property string $status_survey_pengumuman
 * @property string $periode2_kode_ptngo
 * @property string $periode3_username_teman
 *
 * @property Transaksi $transaksi
 * @property PeriodeJenis $periodeJenis
 * @property PeriodeKota $periodeKota
 * @property Jurusan $jurusan
 * @property Regencies $kota
 * @property Peserta $periode3UsernameTeman
 * @property Peserta[] $pesertas
 * @property Transaksi[] $transaksis
 */
class Peserta extends \technosmart\yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $isDeleted;
    public $id_provinces;
    public $virtual_survey_pembahasan = [];
    public $virtual_survey_pengumuman = [];
    public $virtual_poto_upload;
    public $virtual_poto_download;

    public static function tableName()
    {
        return 'peserta';
    }

    public function rules()
    {
        return [
            //id

            //id_transaksi
            [['id_transaksi'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_transaksi'], 'integer'],
            [['id_transaksi'], 'exist', 'skipOnError' => true, 'targetClass' => Transaksi::className(), 'targetAttribute' => ['id_transaksi' => 'id']],

            //peserta_utama
            [['peserta_utama'], 'string'],

            //username
            [['username'], 'trim', 'when' => function($model) {
                return $model->username != NULL;
            }],
            [['username'], 'unique', 'message' => 'Username ini sudah pernah dipakai.'],
            [['username'], 'string', 'min' => 2, 'max' => 16],
            [['username'], 'required'],

            //password
            [['password'], 'required'],
            [['password'], 'string', 'min' => 6, 'max' => 16],

            //kode
            [['kode'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['kode'], 'integer'],
            [['kode'], 'unique'],

            //nama
            [['nama'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['nama'], 'string', 'max' => 256],

            //email
            [['email'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['email'], 'string', 'max' => 256],
            [['email'], 'match', 'pattern' => '/^[\w.+\-]+@(aol.com|gmail.com|hotmail.com|live.com|outlook.com|[\w.+\-]+sch.id|yahoo.co.id|yahoo.com|ymail.com)$/', 'message' => '{attribute} yang diperbolehkan hanya aol.com, gmail.com, hotmail.com, live.com, outlook.com, sch.id, co.id, yahoo.com, ymail.com'],

            //handphone
            [['handphone'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['handphone'], 'number', 'message' => '{attribute} harus angka'],
            [['handphone'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['handphone'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //id_periode_jenis
            [['id_periode_jenis'], 'integer'],
            [['id_periode_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeJenis::className(), 'targetAttribute' => ['id_periode_jenis' => 'id']],

            //harga
            // [['harga'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['harga'], 'integer'],

            //periode_penjualan
            // [['periode_penjualan'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['periode_penjualan'], 'string', 'max' => 256],

            //id_periode_kota
            [['id_periode_kota'], 'integer'],
            [['id_periode_kota'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeKota::className(), 'targetAttribute' => ['id_periode_kota' => 'id']],

            //jenis_kelamin
            [['jenis_kelamin'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['jenis_kelamin'], 'string'],

            //sekolah
            [['sekolah'], 'string', 'max' => 256],

            //id_kota
            [['id_kota'], 'string', 'max' => 4],
            [['id_kota'], 'exist', 'skipOnError' => true, 'targetClass' => \technosmart\modules\location\models\Regencies::className(), 'targetAttribute' => ['id_kota' => 'id']],

            //alamat
            [['alamat'], 'string', 'max' => 256],

            //facebook
            [['facebook'], 'string', 'max' => 256],

            //twitter
            [['twitter'], 'string', 'max' => 256],

            //instagram
            [['instagram'], 'string', 'max' => 256],

            //line
            [['line'], 'string', 'max' => 256],

            //whatsapp
            [['whatsapp'], 'number', 'message' => '{attribute} harus angka'],
            [['whatsapp'], 'match', 'pattern' => '/^08.*/', 'message' => '{attribute} harus diawali dengan 08'],
            [['whatsapp'], 'string', 'min' => 7, 'max' => 17, 'tooShort' => '{attribute} minimal 7 angka', 'tooLong' => '{attribute} maksimal 17 angka'],

            //id_jurusan
            [['id_jurusan'], 'integer'],
            [['id_jurusan'], 'exist', 'skipOnError' => true, 'targetClass' => Jurusan::className(), 'targetAttribute' => ['id_jurusan' => 'id']],

            //status_survey_pembahasan
            [['status_survey_pembahasan'], 'string'],

            //status_survey_pengumuman
            [['status_survey_pengumuman'], 'string'],

            //periode2_kode_ptngo
            [['periode2_kode_ptngo'], 'string', 'max' => 32],

            //periode3_username_teman
            [['periode3_username_teman'], 'string', 'max' => 16],
            [['periode3_username_teman'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['periode3_username_teman' => 'username']],

            //virtual_survey_pembahasan
            [['virtual_survey_pembahasan'], 'safe'],
            // [['virtual_survey_pembahasan'], 'each', 'rule' => ['required', 'on' => 'pembahasan', 'message' => '{attribute} tidak boleh kosong'], 'allowMessageFromRule' => 'false'],
            
            //virtual_survey_pengumuman
            [['virtual_survey_pengumuman'], 'safe'],
            // [['virtual_survey_pengumuman'], 'each', 'rule' => ['required', 'on' => 'pembahasan', 'message' => '{attribute} tidak boleh kosong'], 'allowMessageFromRule' => 'false'],

            //id_provinces
            [['id_provinces'], 'integer'],

            //virtual_poto_download
            [['virtual_poto_download'], 'safe'],
            
            //virtual_poto_upload
            [['virtual_poto_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal screenshot bukti bayar adalah 3MB'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_poto_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_poto_upload');
        if ($this->virtual_poto_upload) {
            $this->poto = $this->virtual_poto_upload->name;
            // $this->poto = $this->kode . '.' . $this->virtual_poto_upload->extension;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_poto_upload) {
            if (!$insert) {
                $fileRoot = '@upload-peserta-poto';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->poto;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_poto_upload) {
            $uploadRoot = '@upload-peserta-poto';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_poto_upload->saveAs($path . '/' . $this->poto);
        }
    }*/

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = '@upload-peserta-poto';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->poto;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    /*public function afterFind()
    {
        parent::afterFind();
        
        if($this->poto) {
            $downloadBaseUrl = '@download-peserta-poto';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_poto_download = $path . '/' . $this->poto;
        }
    }*/

    public static function findByUsername($username)
    {
        return static::find()
            ->join('INNER JOIN', 'transaksi t', 't.id = peserta.id_transaksi')
            ->where('username = :username', [':username' => $username])
            ->andWhere(['t.status_aktif' => 'Aktif'])
            ->andWhere(['t.id_periode' => (Periode::getPeriodeAktif())->id])
            ->one();
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id/*, 'status' => 'Aktif'*/]);
    }

    /**
     * Login secara API based
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['password' => $token/*, 'status' => 'Aktif'*/]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * Membandingkan Authkey existing dengan Authkey yang ada di browser
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->virtual_poto_upload) {
            $uploadRoot = '@upload-peserta-poto';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_poto_upload->saveAs($path . '/' . $this->poto);
        }
        
        \Yii::$app->db->createCommand()->delete('peserta_survey_pembahasan', 'id_peserta = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_survey_pembahasan))
            foreach ($this->virtual_survey_pembahasan as $key => $value) {
                $psp = new PesertaSurveyPembahasan();
                $psp->id_peserta = $this->id;
                $psp->id_survey_pembahasan = $key;
                $psp->jawaban = $value;
                $psp->peserta_utama = 'Tidak';
                $psp->save();
            }
        
        \Yii::$app->db->createCommand()->delete('peserta_survey_pengumuman', 'id_peserta = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_survey_pengumuman))
            foreach ($this->virtual_survey_pengumuman as $key => $value) {
                $psp = new PesertaSurveyPengumuman();
                $psp->id_peserta = $this->id;
                $psp->id_survey_pengumuman = $key;
                $psp->jawaban = $value;
                $psp->peserta_utama = 'Tidak';
                $psp->save();
            }
    }

    public function afterFind()
    {
        parent::afterFind();

        if($this->poto) {
            $downloadBaseUrl = '@download-peserta-poto';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_poto_download = $path . '/' . $this->poto;
        }
        
        $this->virtual_survey_pembahasan = \yii\helpers\ArrayHelper::map(\app_tryout\models\PesertaSurveyPembahasan::find()
            ->select(['sp.id', 'jawaban'])
            ->join('RIGHT JOIN', 'survey_pembahasan sp', 'sp.id = peserta_survey_pembahasan.id_survey_pembahasan AND id_peserta=' . $this->id)
            ->where(['peserta_survey_pembahasan.peserta_utama' => 'Tidak'])
            ->indexBy('id')
            ->asArray()
            ->all(),
        'id', 'jawaban');
        
        $this->virtual_survey_pengumuman = \yii\helpers\ArrayHelper::map(\app_tryout\models\PesertaSurveyPengumuman::find()
            ->select(['sp.id', 'jawaban'])
            ->join('RIGHT JOIN', 'survey_pengumuman sp', 'sp.id = peserta_survey_pengumuman.id_survey_pengumuman AND id_peserta=' . $this->id)
            ->where(['peserta_survey_pengumuman.peserta_utama' => 'Tidak'])
            ->indexBy('id')
            ->asArray()
            ->all(),
        'id', 'jawaban');

        if ($this->id_kota) {
            $this->id_provinces = $this->kota->province_id;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_transaksi' => 'Transaksi',
            'peserta_utama' => 'Peserta Utama',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'email' => 'Email',
            'handphone' => 'Handphone',
            'id_periode_jenis' => 'Periode Jenis',
            'harga' => 'Harga',
            'periode_penjualan' => 'Periode Penjualan',
            'id_periode_kota' => 'Periode Kota',
            'jenis_kelamin' => 'Jenis Kelamin',
            'sekolah' => 'Sekolah',
            'id_kota' => 'Kota',
            'alamat' => 'Alamat',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'line' => 'Line',
            'whatsapp' => 'Whatsapp',
            'id_jurusan' => 'Jurusan',
            'status_survey_pembahasan' => 'Status Survey Pembahasan',
            'status_survey_pengumuman' => 'Status Survey Pengumuman',
            'periode2_kode_ptngo' => 'Kode Ptngo',
            'periode3_username_teman' => 'Periode3 Username Teman',
            'virtual_survey_pembahasan' => 'Jawaban',
            'virtual_survey_pengumuman' => 'Jawaban',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksi()
    {
        return $this->hasOne(Transaksi::className(), ['id' => 'id_transaksi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeJenis()
    {
        return $this->hasOne(PeriodeJenis::className(), ['id' => 'id_periode_jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeKota()
    {
        return $this->hasOne(PeriodeKota::className(), ['id' => 'id_periode_kota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJurusan()
    {
        return $this->hasOne(Jurusan::className(), ['id' => 'id_jurusan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKota()
    {
        return $this->hasOne(\technosmart\modules\location\models\Regencies::className(), ['id' => 'id_kota']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode3UsernameTeman()
    {
        return $this->hasOne(Peserta::className(), ['username' => 'periode3_username_teman']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['periode3_username_teman' => 'username']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksis()
    {
        return $this->hasMany(Transaksi::className(), ['id_duta' => 'id']);
    }
}
