<?php
namespace app_tryout\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class User extends \technosmart\models\User
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            //username
            [['username'], 'required'],
            
            //email
            [['email'], 'required'],
        ]);
    }

    public static function findByLogin($login)
    {
        return static::find()
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->one();
    }

    public function getUser()
    {
        return $this->hasOne(UserExtend::className(), ['id' => 'id']);
    }
}