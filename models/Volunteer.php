<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "volunteer".
 *
 * @property integer $id
 * @property integer $id_pic
 * @property string $nama
 * @property string $email
 * @property string $handphone
 * @property string $ukuran_kaos
 * @property string $uraian_pekerjaan
 *
 * @property Pic $pic
 */
class Volunteer extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'volunteer';
    }

    public function rules()
    {
        return [
            //id

            //id_pic
            [['id_pic'], 'required'],
            [['id_pic'], 'integer'],
            [['id_pic'], 'exist', 'skipOnError' => true, 'targetClass' => Pic::className(), 'targetAttribute' => ['id_pic' => 'id']],

            //nama
            [['nama'], 'string', 'max' => 256],

            //email
            [['email'], 'required'],
            [['email'], 'string', 'max' => 256],

            //handphone
            [['handphone'], 'string', 'max' => 16],

            //ukuran_kaos
            [['ukuran_kaos'], 'string', 'max' => 4],

            //uraian_pekerjaan
            [['uraian_pekerjaan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pic' => 'Pic',
            'nama' => 'Nama',
            'email' => 'Email',
            'handphone' => 'Handphone',
            'ukuran_kaos' => 'Ukuran Kaos',
            'uraian_pekerjaan' => 'Uraian Pekerjaan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPic()
    {
        return $this->hasOne(Pic::className(), ['id' => 'id_pic']);
    }
}
