<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "periode_metode_pembayaran".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property string $nama
 *
 * @property Periode $periode
 * @property Peserta[] $pesertas
 */
class PeriodeMetodePembayaran extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'periode_metode_pembayaran';
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 32],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Id Periode',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['id_periode_metode_pembayaran' => 'id']);
    }
}
