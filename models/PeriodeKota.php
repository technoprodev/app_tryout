<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "periode_kota".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property string $id_regencies
 * @property string $kode
 * @property string $nama
 * @property string $status
 * @property string $alamat
 * @property string $link_google_maps
 * @property integer $kuota
 * @property integer $total_saintek_yang_datang
 * @property string $saintek
 * @property string $catatan_saintek
 * @property integer $total_soshum_yang_datang
 * @property string $soshum
 * @property string $catatan_soshum
 * @property integer $jumlah_volunteer_hari_h
 * @property string $lembar_kode_peserta_dipakai
 * @property string $dimana_lembar_kode_peserta_kamu
 * @property string $kejadian_aneh_hari_h
 * @property string $cerita_curhat_kritik_saran
 * @property string $laporan_kegiatan_sudah_final
 *
 * @property Periode $periode
 * @property Regencies $regencies
 * @property Peserta[] $pesertas
 * @property PesertaTambahan[] $pesertaTambahans
 * @property Pic[] $pics
 */
class PeriodeKota extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'periode_kota';
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //id_regencies
            [['id_regencies'], 'required'],
            [['id_regencies'], 'string', 'max' => 4],
            [['id_regencies'], 'exist', 'skipOnError' => true, 'targetClass' => \technosmart\modules\location\models\Regencies::className(), 'targetAttribute' => ['id_regencies' => 'id']],

            //kode
            [['kode'], 'required'],
            [['kode'], 'string', 'max' => 4],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],

            //status
            [['status'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['status'], 'string'],

            //alamat
            [['alamat'], 'string'],

            //link_google_maps
            [['link_google_maps'], 'string'],

            //kuota
            [['kuota'], 'required'],
            [['kuota'], 'integer'],

            //total_saintek_yang_datang
            [['total_saintek_yang_datang'], 'integer'],

            //saintek
            [['saintek'], 'string'],

            //catatan_saintek
            [['catatan_saintek'], 'string'],

            //total_soshum_yang_datang
            [['total_soshum_yang_datang'], 'integer'],

            //soshum
            [['soshum'], 'string'],

            //catatan_soshum
            [['catatan_soshum'], 'string'],

            //jumlah_volunteer_hari_h
            [['jumlah_volunteer_hari_h'], 'integer'],

            //lembar_kode_peserta_dipakai
            [['lembar_kode_peserta_dipakai'], 'string'],

            //dimana_lembar_kode_peserta_kamu
            [['dimana_lembar_kode_peserta_kamu'], 'string'],

            //kejadian_aneh_hari_h
            [['kejadian_aneh_hari_h'], 'string'],

            //cerita_curhat_kritik_saran
            [['cerita_curhat_kritik_saran'], 'string'],

            //laporan_kegiatan_sudah_final
            [['laporan_kegiatan_sudah_final'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Periode',
            'id_regencies' => 'Regencies',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'status' => 'Status',
            'alamat' => 'Alamat',
            'link_google_maps' => 'Link Google Maps',
            'kuota' => 'Kuota',
            'total_saintek_yang_datang' => 'Total Saintek Yang Datang',
            'saintek' => 'Saintek',
            'catatan_saintek' => 'Catatan Saintek',
            'total_soshum_yang_datang' => 'Total Soshum Yang Datang',
            'soshum' => 'Soshum',
            'catatan_soshum' => 'Catatan Soshum',
            'jumlah_volunteer_hari_h' => 'Jumlah Volunteer Hari H',
            'lembar_kode_peserta_dipakai' => 'Lembar Kode Peserta Dipakai',
            'dimana_lembar_kode_peserta_kamu' => 'Dimana Lembar Kode Peserta Kamu',
            'kejadian_aneh_hari_h' => 'Kejadian Aneh Hari H',
            'cerita_curhat_kritik_saran' => 'Cerita/Curhat/Kritik/Saran',
            'laporan_kegiatan_sudah_final' => 'Laporan Kegiatan Sudah Final ?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegencies()
    {
        return $this->hasOne(\technosmart\modules\location\models\Regencies::className(), ['id' => 'id_regencies']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['id_periode_kota' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaTambahans()
    {
        return $this->hasMany(PesertaTambahan::className(), ['id_periode_kota' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPics()
    {
        return $this->hasMany(Pic::className(), ['id_periode_kota' => 'id']);
    }
}
