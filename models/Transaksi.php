<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "transaksi".
 *
 * @property integer $id
 * @property integer $id_periode
 * @property string $status_bayar
 * @property string $pendaftaran_melalui
 * @property string $created_at
 * @property string $updated_at
 * @property string $confirm_request_at
 * @property string $confirm_at
 * @property string $status_aktif
 * @property string $nonaktif_at
 * @property integer $id_duta
 * @property integer $kode_referral_agent
 * @property integer $id_sumber
 * @property integer $jumlah_tiket
 * @property integer $tagihan
 * @property integer $id_periode_metode_pembayaran
 * @property string $tanggal_pembayaran
 * @property string $pembayaran_atas_nama
 * @property string $bukti_pembayaran
 * @property string $catatan_pembayaran
 * @property string $catatan
 * @property string $catatan_gratis
 * @property integer $resend_pendaftaran_1
 * @property string $resend_pendaftaran_1_date
 * @property integer $resend_pendaftaran_2
 * @property string $resend_pendaftaran_2_date
 *
 * @property Peserta[] $pesertas
 * @property Periode $periode
 * @property Sumber $sumber
 * @property PeriodeMetodePembayaran $periodeMetodePembayaran
 * @property Peserta $duta
 * @property ReferralAgent $kodeReferralAgent
 */
class Transaksi extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_bukti_pembayaran_upload;
    public $virtual_bukti_pembayaran_download;

    public static function tableName()
    {
        return 'transaksi';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //id_periode
            [['id_periode'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['id_periode'], 'integer'],
            [['id_periode'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::className(), 'targetAttribute' => ['id_periode' => 'id']],

            //status_bayar
            [['status_bayar'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['status_bayar'], 'string'],

            //pendaftaran_melalui
            [['pendaftaran_melalui'], 'string'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //confirm_request_at
            [['confirm_request_at'], 'safe'],

            //confirm_at
            [['confirm_at'], 'safe'],

            //status_aktif
            [['status_aktif'], 'string'],

            //nonaktif_at
            [['nonaktif_at'], 'safe'],

            //id_duta
            [['id_duta'], 'integer'],
            [['id_duta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['id_duta' => 'id']],

            //kode_referral_agent
            [['kode_referral_agent'], 'integer'],
            [['kode_referral_agent'], 'exist', 'skipOnError' => true, 'targetClass' => ReferralAgent::className(), 'targetAttribute' => ['kode_referral_agent' => 'id']],

            //id_sumber
            [['id_sumber'], 'integer'],
            [['id_sumber'], 'exist', 'skipOnError' => true, 'targetClass' => Sumber::className(), 'targetAttribute' => ['id_sumber' => 'id']],

            //jumlah_tiket
            [['jumlah_tiket'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['jumlah_tiket'], 'integer'],

            //tagihan
            [['tagihan'], 'required', 'message' => '{attribute} tidak boleh kosong'],
            [['tagihan'], 'integer'],

            //id_periode_metode_pembayaran
            [['id_periode_metode_pembayaran'], 'integer'],
            [['id_periode_metode_pembayaran'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodeMetodePembayaran::className(), 'targetAttribute' => ['id_periode_metode_pembayaran' => 'id']],

            //tanggal_pembayaran
            [['tanggal_pembayaran'], 'safe'],

            //pembayaran_atas_nama
            [['pembayaran_atas_nama'], 'string', 'max' => 256],

            //bukti_pembayaran
            [['bukti_pembayaran'], 'string', 'max' => 256],

            //catatan_pembayaran
            [['catatan_pembayaran'], 'string'],

            //catatan
            [['catatan'], 'string'],

            //catatan_gratis
            [['catatan_gratis'], 'string'],

            //resend_pendaftaran_1
            [['resend_pendaftaran_1'], 'integer'],

            //resend_pendaftaran_1_date
            [['resend_pendaftaran_1_date'], 'safe'],

            //resend_pendaftaran_2
            [['resend_pendaftaran_2'], 'integer'],

            //resend_pendaftaran_2_date
            [['resend_pendaftaran_2_date'], 'safe'],

            //virtual_bukti_pembayaran_download
            [['virtual_bukti_pembayaran_download'], 'safe'],
            
            //virtual_bukti_pembayaran_upload
            [['virtual_bukti_pembayaran_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf', 'maxSize' => 3000 * 1024, 'tooBig' => 'Maksimal screenshot bukti bayar adalah 3MB'],

            //konfirmasi
            [['id_periode_metode_pembayaran', 'tanggal_pembayaran', 'pembayaran_atas_nama', 'virtual_bukti_pembayaran_upload'], 'required', 'message' => '{attribute} tidak boleh kosong', 'on' => 'konfirmasi'],

            //custom
            [['id_sumber'], 'required', 'message' => '{attribute} tidak boleh kosong'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->virtual_bukti_pembayaran_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_pembayaran_upload');
        if ($this->virtual_bukti_pembayaran_upload) {
            $this->bukti_pembayaran = $this->virtual_bukti_pembayaran_upload->name;
            // $this->bukti_pembayaran = $this->kode . '.' . $this->virtual_bukti_pembayaran_upload->extension;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->virtual_bukti_pembayaran_upload) {
            if (!$insert) {
                $fileRoot = '@upload-transaksi-bukti_pembayaran';
                $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_pembayaran;
                if (is_file($filePath)) unlink($filePath);
            }
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_bukti_pembayaran_upload) {
            $uploadRoot = '@upload-transaksi-bukti_pembayaran';
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_pembayaran_upload->saveAs($path . '/' . $this->bukti_pembayaran);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $fileRoot = '@upload-transaksi-bukti_pembayaran';
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->bukti_pembayaran;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->bukti_pembayaran) {
            $downloadBaseUrl = '@download-transaksi-bukti_pembayaran';
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_pembayaran_download = $path . '/' . $this->bukti_pembayaran;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_periode' => 'Periode',
            'status_bayar' => 'Status Bayar',
            'pendaftaran_melalui' => 'Pendaftaran Melalui',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'confirm_request_at' => 'Confirm Request At',
            'confirm_at' => 'Confirm At',
            'status_aktif' => 'Status Aktif',
            'nonaktif_at' => 'Nonaktif At',
            'id_duta' => 'Duta',
            'kode_referral_agent' => 'Kode Referral Agent',
            'id_sumber' => 'Sumber',
            'jumlah_tiket' => 'Jumlah Tiket',
            'tagihan' => 'Tagihan',
            'id_periode_metode_pembayaran' => 'Periode Metode Pembayaran',
            'tanggal_pembayaran' => 'Tanggal Pembayaran',
            'pembayaran_atas_nama' => 'Pembayaran Atas Nama',
            'bukti_pembayaran' => 'Bukti Pembayaran',
            'catatan_pembayaran' => 'Catatan Pembayaran',
            'catatan' => 'Catatan',
            'catatan_gratis' => 'Catatan Gratis',
            'resend_pendaftaran_1' => 'Resend Pendaftaran 1',
            'resend_pendaftaran_1_date' => 'Resend Pendaftaran 1 Date',
            'resend_pendaftaran_2' => 'Resend Pendaftaran 2',
            'resend_pendaftaran_2_date' => 'Resend Pendaftaran 2 Date',
            'virtual_survey_pembahasan' => 'Jawaban',
            'virtual_survey_pengumuman' => 'Jawaban',
            'virtual_bukti_pembayaran_download' => 'Bukti Pembayaran',
            'virtual_bukti_pembayaran_upload' => 'Bukti Pembayaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['id_transaksi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::className(), ['id' => 'id_periode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSumber()
    {
        return $this->hasOne(Sumber::className(), ['id' => 'id_sumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodeMetodePembayaran()
    {
        return $this->hasOne(PeriodeMetodePembayaran::className(), ['id' => 'id_periode_metode_pembayaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDuta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'id_duta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodeReferralAgent()
    {
        return $this->hasOne(ReferralAgent::className(), ['id' => 'kode_referral_agent']);
    }
}
