<?php
namespace app_tryout\models;

use Yii;
use yii\base\Model;
use app_tryout\models\Peserta;

class LoginPeserta extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    public $peserta;

    public function rules()
    {
        return [
            ['username', 'required', 'on' => 'using-username'],
            
            ['password', 'required'],
            ['password', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'password' => 'Password',
            'rememberMe' => 'Remember Me',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if ($peserta = $this->getPeserta()) {
            if (!$peserta->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        } else {
            switch ($this->scenario) {
                case 'using-username':
                    $this->addError('username', 'Username is not exists.');
                    break;
                default:
                    $this->addError('username', 'Username is not exists.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getPeserta(), $this->rememberMe ? Yii::$app->params['user.passwordResetTokenExpire'] * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    protected function getPeserta()
    {
        if ($this->peserta === null) {
            switch ($this->scenario) {
                case 'using-username':
                    $this->peserta = Peserta::findByUsername($this->username);
                    break;
                default:
                    $this->peserta = Peserta::findByUsername($this->username);
            }
        }

        return $this->peserta;
    }
}