<?php
namespace app_tryout\models;

use Yii;
use app_tryout\models\User;

class LoginAdmin extends \technosmart\models\Login
{
    protected function getUser()
    {
        if ($this->user === null) {
            switch ($this->scenario) {
                case 'using-login':
                    $this->user = User::findByLogin($this->login);
                    break;
                case 'using-username':
                    $this->user = User::findByUsername($this->username);
                    break;
                case 'using-email':
                    $this->user = User::findByEmail($this->email);
                    break;
                case 'using-phone':
                    $this->user = User::findByPhone($this->phone);
                    break;
                default:
                    $this->user = User::findByLogin($this->login);
            }
        }

        return $this->user;
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->userAdmin->login($this->getUser(), $this->rememberMe ? Yii::$app->params['user.passwordResetTokenExpire'] * 24 * 30 : 0);
        } else {
            return false;
        }
    }
}