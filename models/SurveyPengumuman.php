<?php
namespace app_tryout\models;

use Yii;

/**
 * This is the model class for table "survey_pengumuman".
 *
 * @property integer $id
 * @property string $pertanyaan
 * @property string $tipe_pertanyaan
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $e
 *
 * @property PesertaSurveyPengumuman[] $pesertaSurveyPengumumans
 */
class SurveyPengumuman extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'survey_pengumuman';
    }

    public function rules()
    {
        return [
            //id

            //pertanyaan
            [['pertanyaan'], 'required'],
            [['pertanyaan'], 'string'],

            //tipe_pertanyaan
            [['tipe_pertanyaan'], 'required'],
            [['tipe_pertanyaan'], 'string'],

            //a
            [['a'], 'string'],

            //b
            [['b'], 'string'],

            //c
            [['c'], 'string'],

            //d
            [['d'], 'string'],

            //e
            [['e'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pertanyaan' => 'Pertanyaan',
            'tipe_pertanyaan' => 'Tipe Pertanyaan',
            'a' => 'A',
            'b' => 'B',
            'c' => 'C',
            'd' => 'D',
            'e' => 'E',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaSurveyPengumumans()
    {
        return $this->hasMany(PesertaSurveyPengumuman::className(), ['id_survey_pengumuman' => 'id']);
    }
}
